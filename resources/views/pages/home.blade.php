@extends('layout')
@section('home')
    <div class="featured">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell small-12 large-8 news">
                    <div class="featured-news featured-block-1">
                        <a href="newspost" class="overlay-featured"></a>
                        <div class="text">
                            <h3>THE MAN OF STEEL</h3>
                            <p>Bendis is back drawing your favorite hero.</p>
                        </div>
                    </div>
                </div>
                <div class="cell small-12 large-4 ">
                    <div class="grid-x">
                        <div class="cell large-12 news">
                            <div class="featured-news featured-block-2">
                                <a class="overlay-featured">
                                </a>
                                <div class="text">
                                <h3>THE FLASH WAR</h3>
                                <p>Wally is remembering everything of his lost life.</p>
                                </div>
                            </div>
                        </div>
                        <div class="cell large-12 news">
                            <div class="featured-news featured-block-3">
                                <a class="overlay-featured">
                                </a>
                                <div class="text">
                                <h3>BATMAN PRELUDE TO THE WEDDING</h3>
                                <p>Joker, Ra's al Ghul, Anarky and The Riddler are coming!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="featured-comics">
<h1 class="featured-comics-heading">FEATURED COMICS</h1>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12 medium-6 large-3">
                <div class="featured-block">
                    <div class="featured-heading heading-cover-1">
                        <img alt="Batman #50 Comicbook cover" class="cover" src="{{ asset('img/batman-50.jpg') }} ">
                    </div>
                    <div class="featured-text">
                        <p class="heading-text">BATMAN MARRIES CATWOMAN</p>
                        <p class="sub-heading">BATMAN #50</p>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 large-3">
                <div class="featured-block">
                    <div class="featured-heading heading-cover-2">
                        <img alt="Flash Annual #1 Comicbook cover" class="cover" src="{{ asset('img/flash-annual.jpg') }} ">
                    </div>
                    <div class="featured-text">
                        <p class="heading-text">BIGGEST EVENT IN FLASH HISTORY</p>
                        <p class="sub-heading">THE FLASH ANNUAL #1</p>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 large-3">
                <div class="featured-block">
                    <div class="featured-heading heading-cover-3">
                        <img alt="Bomshells United #19 Comicbook cover" class="cover" src="{{ asset('img/bombshells-19.jpg') }} ">
                    </div>
                    <div class="featured-text">
                        <p class="heading-text">WWII DRAMA COMING TO AN END</p>
                        <p class="sub-heading">BOMBSHELLS UNITED #19</p>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 large-3">
                <div class="featured-block">
                    <div class="featured-heading heading-cover-4">
                        <img alt="Justice League United #1 Comicbook cover" class="cover" src="{{ asset('img/justice-league.jpg') }} ">
                    </div>
                    <div class="featured-text">
                        <p class="heading-text">JUSTICE LEAGUE UNITE</p>
                        <p class="sub-heading">JUSTICE LEAGUE #1</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="heroes">
        <div class="grid-container">
            <h2>MEET THE JUSTICE LEAGUE</h2>
            <div class="grid-x">
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="Superman" class="hero-pf" src="{{ asset('img/superman.jpg') }}">
                    <p>Superman</p>
                </div>
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="Batman" class="hero-pf" src="{{ asset('img/batman.jpg') }}">
                    <p>Batman</p>
                </div>
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="Wonder Woman" class="hero-pf" src="{{ asset('img/wonderwoman.jpg') }}">
                    <p>Wonder Woman</p>
                </div>
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="Green Lantern" class="hero-pf" src="{{ asset('img/greenlantern.jpg') }}">
                    <p>Green Lantern</p>
                </div>
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="The Flash" class="hero-pf" src="{{ asset('img/flash.jpg') }}">
                    <p>The Flash</p>
                </div>
                <div class="cell small-12 medium-4 large-2 hero">
                    <img alt="Aquaman" class="hero-pf" src="{{ asset('img/aquaman.jpg') }}">
                    <p>Aquaman</p>
                </div>
            </div>
        </div>
    </div>

@endsection


