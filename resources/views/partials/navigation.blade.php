<div class="navigation">
    <div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle="example-menu"></button>
        <div class="title-bar-title">Menu</div>
    </div>
    <div class="top-bar" id="example-menu">
        <div class="logo-bar">
            <li class="menu-text"><img class="logo" src="{{ asset('img/logo.png') }}"></li>
        </div>
        <div class="top-bar-left">
            <ul class="dropdown menu" data-dropdown-menu>
                <li class="nav-item"><a href="/">HOME</a></li>
                <li class="nav-item"><a href="news">NEWS</a></li>
                <li class="nav-item"><a href="your-comics">YOUR COMICS</a></li>
            </ul>
        </div>
        <div class="top-bar-right">

            <ul class="dropdown menu" data-dropdown-menu>
                <li>
                    <a class="profile" >
                        <img class="pf" src="{{ asset('img/pf.png') }}">
                        <span>Lycan</span>
                    </a>
                    <ul class="menu">
                        <li class="menu-item"><a href="profile"><i class="fas fa-user-alt"></i><span class="menu-item">Profile</span></a></li>
                        <li class="menu-item"><a href="#"><i class="fas fa-cogs"></i><span class="menu-item">Settings</span></a></li>
                        <li class="menu-item"><a href="#"><i class="fas fa-sign-out-alt"></i><span class="menu-item">Sign out</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
