<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="The DC Comic Collector where you can make your own Comic Book collection list so that you can keep track of all your DC Comic Book Issues and read about the latest news from the world of DC Comics">
    <meta name="keywords" content="Laravel, Foundation, jQuery, DC Comics, Comic Book Collector, The Flash, Batman, Superman, Aqua Man, News, Collection">
    <meta name="author" content="Peter Boersma">
    <meta property="og:title" content="DC | Comic Collector" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="The DC Comic Collector where you can make your own Comic Book collection list so that you can keep track of all your DC Comic Book Issues and read about the latest news from the world of DC Comics" />
    <meta property="og:url" content="http://www.frox.lycan-media.nl/" />
    <meta property="og:image" content="http://www.frox.lycan-media.nl/img/logo.png" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@frox.lycan-media.nl" />
    <meta name="twitter:creator" content="@LycanMD" />

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/favicons/android-icon-192x192.png">
    <link rel="mask-icon" href="img/favicons/favicon.svg" color="blue">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
    <link rel="manifest" href="img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>DC | Comicbook Collector</title>
    <link rel="stylesheet" type="text/css" href="css/app.css"  />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/flickity.css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106869946-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-106869946-3');
    </script>

</head>
<body>
@include('partials/navigation')
@yield('home')
@yield('news')
@yield('newspost')
@yield('ycomics')
@yield('profile')

@include('partials/footer')

<script src="/js/flickity.min.js"></script>
<script src="/js/app.js"></script>
<script> $(document).foundation();</script>


</body>
</html>
