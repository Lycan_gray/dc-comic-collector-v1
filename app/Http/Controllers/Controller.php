<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        return view('pages.home');
    }

    public function GTNews(){
        return view('pages.news');
    }
    public function NewsPost(){
        return view('pages.newspost');
    }
    public function yComics(){
        return view('pages.ycomics');
    }
    public function Profile(){
        return view('pages.profile');
    }
}
